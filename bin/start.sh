#!/bin/bash
#Author: Andrew Snow

# Launch the Flask app in virtualenv
cd ..
sleep 1s
virtualenv env
sleep 1s
source env/bin/activate
sleep 1s
python app.py

