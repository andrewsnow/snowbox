# Snowbox Flask App

# Importing necessary lib and utils
import os
from flask import Flask, render_template, redirect, request, url_for, send_from_directory
from werkzeug.utils import secure_filename

# Some configuration mumbo jumbo
BASEDIR = os.path.abspath(os.path.dirname(__file__))
UPLOAD_FOLDER = '%s/uploads' % BASEDIR
app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['CUSTOM_STATIC_PATH'] = UPLOAD_FOLDER

# Let's display the index landing page
@app.route('/')
def index():

    # Check if path is a file and serve
    if os.path.isfile(UPLOAD_FOLDER):
        return send_file(UPLOAD_FOLDER)

    # List all files
    files = os.listdir(UPLOAD_FOLDER)

    # Render the index with all the files!
    return render_template('index.html', files=files)

# Custom static file fetcher since we cant serve uploads from the /static folder >.<
@app.route('/uploads/<path:filename>')
def upload_link(filename):
	# Strip any weird characters
    secure = secure_filename(filename)

    # Access the /uploads folder content!
    return send_from_directory(app.config['CUSTOM_STATIC_PATH'], secure)

# Actual uploading of file (based off Flask documentation)
@app.route("/upload", methods=["GET", "POST"])
def upload():
    #Ensure request is a POST method
    if request.method == "POST":
        # Ensure a file is selected
        if 'file' not in request.files:
            print 'No file found in request'
            return redirect(url_for('redirect_me'))

        # Pull file from the request
        file = request.files['file']

        # Ensure a file was posted successfully
        if file.filename == '':
            print 'No file was passed to upload'    
            return redirect(url_for('redirect_me'))

        # Upload the file!
        if file:
            filename = secure_filename(file.filename)
            file_name, file_extension = os.path.splitext(filename)

            # Simple duplicate name handling
            uniq = 1
            saveloc = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            while os.path.exists(saveloc):
                filename = '%s_%d%s' % (file_name, uniq, file_extension)
                saveloc = os.path.join(app.config['UPLOAD_FOLDER'], filename)
                uniq += 1

            # Save the file
            file.save(saveloc)
            print "Uploaded %s successful!" % filename
            message = 'File ' + filename + ' uploaded successfully.'
            return redirect(url_for('index'))
        
        # Return to index because we have to return in a view
        return redirect(url_for('index'))
    return redirect(url_for('index'))

# Run da app on port 9393
if __name__ == '__main__':
    app.run(host='localhost', port=9393)
