Snowbox - Simple drag and drop file uploader

NOTE:
- Browser's automatically handle certain file extensions, e.g. text files, which is why they do not automatically download


Built with:
- Python
- Flask
- DropzoneJS

TODO:
- Make drag and drop area global on landing page
- Transform the uploaded files to look more beautiful